#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <libusb-1.0/libusb.h>

#include <QDebug>
#include <QByteArray>
#include <QPlainTextEdit>
#include <QFont>
#include <QImage>
#include <QTime>

#include "math.h"

void sendUSBData(QByteArray dataToSend, int endpoint, libusb_device_handle *dev_handle) {
    int actual; //used to find out how many bytes were written

    qDebug()<<"Writing Data..."<<endl;
    unsigned char* sendMeReal = (unsigned char*) dataToSend.data();
    int r = libusb_bulk_transfer(dev_handle,  (LIBUSB_ENDPOINT_OUT |  endpoint), sendMeReal, dataToSend.length(), &actual, 0);
    if(r == 0 && actual == dataToSend.length()) //we wrote the bytes successfully
        qDebug()<<"Writing Successful!";
    else
        qDebug()<<"Write Error with actual being " << actual <<" bytes" << endl;
}

QByteArray readUSBData(int endpoint, libusb_device_handle *dev_handle, int readBytes) {
    QByteArray returnMe;
    int actual;

    unsigned char *realBackData = new unsigned char[readBytes]; //data to read

    libusb_bulk_transfer(dev_handle, (LIBUSB_ENDPOINT_IN | endpoint), realBackData, readBytes, &actual, 0);

    for(int i=0;i<actual;i++) {
        returnMe.append(realBackData[i]);
    }


    return returnMe;
}

void handshake(libusb_device_handle *dev_handle,QPlainTextEdit* textEdit) {
    QByteArray sendMe = QByteArray::fromHex("c80601b801b500007530abcd");

    sendUSBData(sendMe,2,dev_handle);

    QByteArray backData = readUSBData(6,dev_handle,16);

    QString hexDataBack = backData.toHex();
    textEdit->setPlainText(textEdit->toPlainText() + "DataBack: " + hexDataBack);
    QString firmware = hexDataBack.right(2);
    textEdit->setPlainText(textEdit->toPlainText() + "\nFirmware version: " + firmware);
    QString sensorType = hexDataBack.mid(14 * 2,2);
    textEdit->setPlainText(textEdit->toPlainText() + "\nSensor Type: " + sensorType);
    QString lotNumber = hexDataBack.mid(11*2,2) + hexDataBack.mid(10*2,2);
    textEdit->setPlainText(textEdit->toPlainText() + "\nLot Number: " + lotNumber);
    QString serialNumber = sensorType + lotNumber;
    textEdit->setPlainText(textEdit->toPlainText() + "\nSerial Number: " + serialNumber);
}

void blackImage(libusb_device_handle *dev_handle,QPlainTextEdit* textEdit) {
    QByteArray sendMe = QByteArray::fromHex("c80601bd01b500e4e1c0abcd"); //frame 1
    sendUSBData(sendMe,2,dev_handle);
    QByteArray backData = readUSBData(6,dev_handle,16);
    QString hexDataBack = backData.toHex();
    textEdit->setPlainText(textEdit->toPlainText() + "DataBack: " + hexDataBack); //frame 2

    sendMe = QByteArray::fromHex("c80601b801b500e4e1c0abcd"); //frame 3
    sendUSBData(sendMe,2,dev_handle);
    backData = readUSBData(6,dev_handle,16);
    hexDataBack = backData.toHex();
    textEdit->setPlainText(textEdit->toPlainText() + "\nDataBack: " + hexDataBack); //frame 4

    sendMe = QByteArray::fromHex("c80301b801b500e4e1c0abcd"); //frame 5
    sendUSBData(sendMe,2,dev_handle);
    backData = readUSBData(6,dev_handle,16);
    hexDataBack = backData.toHex();
    textEdit->setPlainText(textEdit->toPlainText() + "\nDataBack: " + hexDataBack); //frame 6

    backData = readUSBData(6,dev_handle,4435616);
    hexDataBack = backData.toHex();
    textEdit->setPlainText(textEdit->toPlainText() + "\nDataBack: " + hexDataBack.left(100)); //frame 7
    textEdit->setPlainText(textEdit->toPlainText() + "\nLength: " + QString::number(backData.length()));

    QFile saveBlack("black.raw");
    saveBlack.open(QIODevice::WriteOnly | QIODevice::Text);
    saveBlack.write(backData.toHex());
    saveBlack.close();
}


void MainWindow::getTheDarnImage(libusb_device_handle *dev_handle,QPlainTextEdit* textEdit) {
    QByteArray sendMe = QByteArray::fromHex("c80601bd01b500e4e1c0abcd"); //frame 1
    sendUSBData(sendMe,2,dev_handle);
    QByteArray backData = readUSBData(6,dev_handle,16);
    QString hexDataBack = backData.toHex();
    textEdit->setPlainText(textEdit->toPlainText() + "DataBack: " + hexDataBack); //frame 2

    sendMe = QByteArray::fromHex("c80601b801b500e4e1c0abcd"); //frame 3
    sendUSBData(sendMe,2,dev_handle);
    backData = readUSBData(6,dev_handle,16);
    hexDataBack = backData.toHex();
    textEdit->setPlainText(textEdit->toPlainText() + "\nDataBack: " + hexDataBack); //frame 4

    sendMe = QByteArray::fromHex("c80101b801b500e4e1c0abcd"); //frame 5
    sendUSBData(sendMe,2,dev_handle);
    backData = readUSBData(6,dev_handle,16);
    hexDataBack = backData.toHex();
    textEdit->setPlainText(textEdit->toPlainText() + "\nDataBack: " + hexDataBack.left(100)); //frame 6


    backData = readUSBData(6,dev_handle,2097152 + 2097152 + 241312);
    hexDataBack = backData.toHex();
    textEdit->setPlainText(textEdit->toPlainText() + "\nDataBack: " + hexDataBack.left(100)); //frame 7,8,9

    QVector<uint16_t> vals;
    QByteArray imgData =backData;

    for(int i=16;i<imgData.length();i+=2) {
        uint8_t val1 = imgData.at(i);
        uint8_t val2 = imgData.at(i+1);
        uint16_t val = val1 + (256 * val2);
        vals.append(val);
    }


    QVector<uint16_t> sortedVals = vals;
    std::sort(sortedVals.begin(),sortedVals.end());
    uint16_t lowCut = sortedVals.at((int)(sortedVals.length() * .05));
    uint16_t highCut = sortedVals.at((int)(sortedVals.length() * .90));
    uint16_t range = highCut - lowCut;

    qDebug()<<"Count: " << vals.length();
    qDebug()<<"lowCut: " << lowCut;
    qDebug()<<"highCut: " << highCut;
    qDebug()<<"range: " << range;


    QImage imgSave(1300,1706,QImage::Format_Grayscale8);
    QPixmap saveMap(1300,1706);

    int i=0;
    for(int y=0;y<1706;y++) {
        for(int x=0;x<1300;x++) {
            uint16_t val = vals.at(i);
            //qDebug()<<val;
            int pixVal=0;
            if(val > highCut) {
                pixVal = 255;
            }
            else if (val < lowCut) {
                pixVal = 0;
            }
            else {
                qreal fract = ((qreal) val - lowCut) / range ;
                //qDebug()<<fract;
                pixVal =(int) (255 * fract);

            }

            imgSave.setPixelColor(x,y,QColor(pixVal,pixVal,pixVal));
            i++;
        }
    }

    imgSave.invertPixels();
    QPixmap pix = QPixmap::fromImage(imgSave);
    ui->label->setPixmap(pix);

    imgSave.save("test.png");
    saveMap.convertFromImage(imgSave);
    ui->label->setPixmap(saveMap);



}

void MainWindow::doUSBStuff(QPlainTextEdit* textEdit) {
    libusb_device **devs; //pointer to pointer of device, used to retrieve a list of devices
    libusb_device_handle *dev_handle; //a device handle
    libusb_context *ctx = NULL; //a libusb session
    int r; //for return values
    ssize_t cnt; //holding number of devices in list
    r = libusb_init(&ctx); //initialize the library for the session we just declared
    if(r < 0) {
        qDebug()<<"Init Error "<<r<<endl; //there was an error
        return;
    }
    libusb_set_debug(ctx, 3); //set verbosity level to 3, as suggested in the documentation

    cnt = libusb_get_device_list(ctx, &devs); //get the list of devices
    if(cnt < 0) {
        qDebug()<<"Get Device Error"<<endl; //there was an error
        return;
    }
    qDebug()<<cnt<<" Devices in list."<<endl;

    dev_handle = libusb_open_device_with_vid_pid(ctx, 1633, 17408); //these are vendorID and productID I found for my usb device
    if(dev_handle == NULL)
        qDebug()<<"Cannot open device"<<endl;
    else
        qDebug()<<"Device Opened"<<endl;
    libusb_free_device_list(devs, 1); //free the list, unref the devices in it

    if(libusb_kernel_driver_active(dev_handle, 0) == 1) { //find out if kernel driver is attached
        qDebug()<<"Kernel Driver Active"<<endl;
        if(libusb_detach_kernel_driver(dev_handle, 0) == 0) //detach it
            qDebug()<<"Kernel Driver Detached!"<<endl;
    }

    r = libusb_claim_interface(dev_handle, 0); //claim interface 0 (the first) of device (mine had jsut 1)
    if(r < 0) {
        qDebug()<<"Cannot Claim Interface"<<endl;
        return;
    }
    qDebug()<<"Claimed Interface"<<endl;

    //handshake(dev_handle,textEdit);
    blackImage(dev_handle,textEdit);
    //getTheDarnImage(dev_handle,textEdit);



    r = libusb_release_interface(dev_handle, 0); //release the claimed interface
    if(r!=0) {
        qDebug()<<"Cannot Release Interface"<<endl;
        return;
    }
    qDebug()<<"Released Interface"<<endl;

    libusb_close(dev_handle); //close the device we opened
    libusb_exit(ctx); //needs to be called to end the
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QFont f("hello");
    f.setStyleHint(QFont::Monospace);
    ui->plainTextEdit->setFont(f);

    connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(DoStuff()));
}

void MainWindow::DoStuff() {
    doUSBStuff(ui->plainTextEdit);
}

MainWindow::~MainWindow()
{
    delete ui;
}
