#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <libusb-1.0/libusb.h>
#include <QByteArray>
#include <QPlainTextEdit>
#include <QFont>
#include <QImage>
#include <QTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void DoStuff();
    void doUSBStuff(QPlainTextEdit* textEdit);

    void getTheDarnImage(libusb_device_handle *dev_handle,QPlainTextEdit* textEdit);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
